import { validateVersion } from "../version/versionValidators";

/**
 * Checks if a storage manifest
 * @param {*} value Storage manifest to validate
 * @returns {string[]} Reasons why it may be invalid
 */
export function validateStorageManifest(value: any): string[] {
  const errors = [];

  if (typeof value !== "object" || value === null) {
    errors.push("manifest must be an object");
  } else if (!("versions" in value)) {
    errors.push("manifest must have a `versions` key");
  } else {
    const { versions } = value;

    if (!Array.isArray(versions)) {
      errors.push("manifest versions must be an array");
    } else {
      versions.forEach(version => {
        const msg = validateVersion(version);

        if (msg.length > 0) {
          errors.push(...msg);
        }
      });
    }
  }

  return errors;
}
