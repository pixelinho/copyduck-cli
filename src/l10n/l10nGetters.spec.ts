import { Config } from "../config/configTypes";
import * as fsHelpers from "../fs/fsHelpers";
import { getL10nFiles } from "./l10nGetters";

describe("getL10nFiles", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("returns files that match the localization regex only", async () => {
    const src1 = "client";
    const src2 = "server";
    const files = ["index.js", "main.l10n.json", "/nav/nav.l10n.json"];
    const config = {
      l10nRegex: /\.l10n\.json/i,
      include: [src1, src2]
    } as Config;

    jest.spyOn(fsHelpers, "walkDir").mockImplementation((cb, dirPath) => {
      if (dirPath === src1) {
        files.forEach(file => cb(file));
      }

      return Promise.resolve();
    });

    expect(await getL10nFiles(config)).toHaveLength(2);
  });
});
