import { join } from "path";
import { CLIOptions } from "../cli/cliTypes";
import { configOption, defaultConfigPath } from "./configSettings";
import { getCurrentWorkingDirectory } from "../process/processGetters";

/**
 * Returns the absolute path of the configuration file from the CLI options
 * if any, otherwise from the default path
 * @param {?CLIOptions} options CLI options to get the path from
 * @returns {string} Absolute path
 */
export function getConfigPathFromCLI(options: CLIOptions = {}): string {
  let path;

  if (configOption in options) {
    path = options[configOption];
  }

  return join(getCurrentWorkingDirectory(), path || defaultConfigPath);
}
