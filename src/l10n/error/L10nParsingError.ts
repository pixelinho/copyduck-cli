/**
 * Creates an instance representing an error that occurs when parsing a
 * localization file
 * @extends Error
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error}
 */
export default class L10nParsingError extends Error {
  /**
   * Creates an instance of the error
   * @param {string} file Error file
   * @param {string} message Error message
   * @param {string[]} errors Errors that caused the parsing error
   */
  constructor(file: string, message: string, errors: string[] = []) {
    super(message);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, L10nParsingError);
    }

    this.name = "L10nParsingError";
    this.message = `${message}, parsing ${file}`;

    errors.forEach(e => (this.message += `\n  - ${e}`));
  }
}
