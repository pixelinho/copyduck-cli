import { logInfo } from "../log/logStreams";
import { VersionDiff } from "../version/versionTypes";

/**
 * Prints the difference between 2 versions
 * @param {VersionDiff} diff
 */
export function logVersionDiffReport({
  removed,
  created,
  updated
}: VersionDiff): void {
  let str = "";

  if (removed.length > 0) {
    str += `  - ${removed.length} message(s) were removed from the last version`;
  }

  if (created.length > 0) {
    str += `  - ${created.length} message(s) were created since the last version`;
  }

  if (updated.length > 0) {
    str += `  - ${updated.length} message(s) were updated`;
  }

  if (!str) {
    str = "No changes were detected from the previous version";
  }

  str = `
Report:
${str}`;

  logInfo(str);
}
