import { CLIOptions } from "../cli/cliTypes";

/**
 * Returns the path from which the script is run
 * @returns {string} Path
 */
export function getCurrentWorkingDirectory(): string {
  return process.cwd();
}

/**
 * Returns the command line arguments
 * @returns {string[]} Arguments
 */
export function getCLIArguments(): string[] {
  return process.argv;
}
