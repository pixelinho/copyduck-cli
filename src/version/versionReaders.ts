import { promises as fsPromises } from "fs";
import { MessageMap } from "../message/messageTypes";
import { joinStoragePath } from "../storage/storageHelpers";
import { getVersionFilename } from "./versionGetters";
import { Version } from "./versionTypes";

/**
 * Reads a version file
 * @param {Version} version Version of which to read the file
 * @returns {Promise<MessageMap>} A promise that resolves with a message map
 */
export async function readVersionFile(version: Version): Promise<MessageMap> {
  return JSON.parse(
    await fsPromises.readFile(
      joinStoragePath(getVersionFilename(version)),
      "utf8"
    )
  ) as MessageMap;
}
