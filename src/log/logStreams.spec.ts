import { logSuccess, logInfo, logWarning, logError } from "./logStreams";

describe("logSuccess", () => {
  it("writes the value in stdout", () => {
    const spy = jest.spyOn(global.console, "log").mockImplementation();

    logSuccess(1);

    expect(spy).toHaveBeenCalled();

    spy.mockRestore();
  });
});

describe("logInfo", () => {
  it("writes the value in stdout", () => {
    const spy = jest.spyOn(global.console, "log").mockImplementation();

    logInfo(1);

    expect(spy).toHaveBeenCalled();

    spy.mockRestore();
  });
});

describe("logWarning", () => {
  it("writes the value in stdout", () => {
    const spy = jest.spyOn(global.console, "log").mockImplementation();

    logWarning(1);

    expect(spy).toHaveBeenCalled();

    spy.mockRestore();
  });
});

describe("logError", () => {
  it("writes the value in stderr", () => {
    const spy = jest.spyOn(global.console, "error").mockImplementation();

    logError(1);

    expect(spy).toHaveBeenCalled();

    spy.mockRestore();
  });
});
