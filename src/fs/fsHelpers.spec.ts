import { promises as fsPromises, Stats } from "fs";
import * as helpers from "./fsHelpers";

const { isPathDirectory, walkDir } = helpers;
const dirPath = "/dir/";
const dirStats = { isDirectory: () => true } as Stats;
const fileStats = { isDirectory: () => false } as Stats;

describe("isPathDirectory", () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it("returns true if the path is the path of a directory", async () => {
    jest.spyOn(fsPromises, "lstat").mockResolvedValue(dirStats);

    expect(await isPathDirectory(dirPath)).toEqual(true);
  });

  it("returns false if the path is the path of a file", async () => {
    jest.spyOn(fsPromises, "lstat").mockResolvedValue(fileStats);

    expect(await isPathDirectory(dirPath)).toEqual(false);
  });
});

describe("walkDir", () => {
  const path1 = "a";
  const path2 = "b";
  const paths = [path1, path2] as any;

  beforeEach(() => {
    jest.resetModules();
  });

  it("resolves if the path is the path to a directory", next => {
    jest.spyOn(helpers, "isPathDirectory").mockResolvedValue(false);

    walkDir(() => null, dirPath).then(() => {
      expect(true).toEqual(true);

      next();
    });
  });

  it("calls the callback with the file paths", async () => {
    jest.spyOn(fsPromises, "readdir").mockResolvedValue(paths);
    jest
      .spyOn(fsPromises, "lstat")
      .mockResolvedValue(fileStats)
      .mockResolvedValueOnce(dirStats);

    const cb = jest.fn();

    await walkDir(cb, dirPath);

    expect(cb).toHaveBeenNthCalledWith(1, `${dirPath}${path1}`);
    expect(cb).toHaveBeenNthCalledWith(2, `${dirPath}${path2}`);
  });

  it("doesn't call the callback if there is no file in the directory", async () => {
    jest
      .spyOn(fsPromises, "readdir")
      .mockResolvedValue([])
      .mockResolvedValueOnce(paths);
    jest.spyOn(fsPromises, "lstat").mockResolvedValue(dirStats);

    const cb = jest.fn();

    await walkDir(cb, dirPath);

    expect(cb).not.toHaveBeenCalled();
  });
});
