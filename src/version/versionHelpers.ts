import { getStorageVersions } from "../storage/storageGetters";
import { storageVersionCount } from "../storage/storageSettings";
import { StorageManifest } from "../storage/storageTypes";
import { getVersionTimestamp } from "./versionGetters";
import { Version } from "./versionTypes";
import { deleteVersionFile } from "./versionWriters";

/**
 * Remove versions that exceed the storage limit from the manifest and delete
 * their files
 * @param {StorageManifest} storageManifest Storage manifest
 * @returns {Promise<void>} A promise that resolves when the storage manifest
 * is updated and when version files are deleted
 */
export async function clearVersions(
  storageManifest: StorageManifest
): Promise<void> {
  const versions = getStorageVersions(storageManifest);

  versions.sort(sortVersions);

  const versionsToDelete = versions.splice(
    storageVersionCount,
    versions.length - storageVersionCount
  );
  const promises = versionsToDelete.map(version => deleteVersionFile(version));

  return Promise.all(promises).then(() => undefined);
}

/**
 * Sorts versions by most recent first (using their timestamp)
 * @param {Version} version1 Version to compare
 * @param {Version} version2 Version to compare
 * @returns {number} -1 if version1 is newer, 1 if version2 is newer,
 * 0 otherwise
 */
export function sortVersions(version1: Version, version2: Version): number {
  const diff = getVersionTimestamp(version2) - getVersionTimestamp(version1);

  return diff === 0 ? diff : diff / Math.abs(diff);
}
