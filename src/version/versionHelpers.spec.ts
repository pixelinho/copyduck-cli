import { promises as fsPromises } from "fs";
import { storageVersionCount } from "../storage/storageSettings";
import { clearVersions, sortVersions } from "./versionHelpers";
import { getStorageVersions } from "../storage/storageGetters";

describe("clearVersions", () => {
  const filename = "file.json";

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("removes the extra versions from the storage manifest", async () => {
    const storageManifest = { versions: [] };
    const spy = jest.spyOn(fsPromises, "unlink").mockResolvedValue();

    for (let i = 0; i < storageVersionCount + 2; i++) {
      getStorageVersions(storageManifest).push({ timestamp: i, filename });
    }

    await clearVersions(storageManifest);

    expect(getStorageVersions(storageManifest)).toHaveLength(
      storageVersionCount
    );
    expect(getStorageVersions(storageManifest)).not.toContainEqual({
      timestamp: 0,
      filename
    });
  });

  it("deletes the files of the extra versions from the file system", async () => {
    const storageManifest = { versions: [] };
    const spy = jest.spyOn(fsPromises, "unlink").mockResolvedValue();

    for (let i = 0; i < storageVersionCount + 2; i++) {
      getStorageVersions(storageManifest).push({ timestamp: i, filename });
    }

    await clearVersions(storageManifest);

    expect(spy).toHaveBeenCalledTimes(2);
  });
});

describe("sortVersions", () => {
  const filename = "file.json";

  it("returns -1 if version1 is newer", () => {
    const version1 = { timestamp: 3, filename };
    const version2 = { timestamp: 1, filename };

    expect(sortVersions(version1, version2)).toEqual(-1);
  });

  it("returns 1 if version2 is newer", () => {
    const version1 = { timestamp: 1, filename };
    const version2 = { timestamp: 3, filename };

    expect(sortVersions(version1, version2)).toEqual(1);
  });

  it("returns 0 if version1 and version2 have the same timestamp", () => {
    const version1 = { timestamp: 1, filename };
    const version2 = { timestamp: 1, filename };

    expect(sortVersions(version1, version2)).toEqual(0);
  });
});
