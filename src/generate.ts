import { parseCLIOptions } from "./cli/cliParsers";
import { getConfigPathFromCLI } from "./config/configHelpers";
import { parseConfigFile } from "./config/configParsers";
import { getL10nFiles } from "./l10n/l10nGetters";
import { readAllL10nFiles } from "./l10n/l10nReaders";
import { logError, logSuccess, logWarning } from "./log/logStreams";
import { createMessageMap } from "./message/messageMap";
import { logVersionDiffReport } from "./report/reportLoggers";
import { getStorageManifest } from "./storage/storageGetters";
import { writeNewVersionInStorage } from "./storage/storageWriters";
import { getLastVersionDiff } from "./version/versionDiffs";

(async () => {
  try {
    // Read and parse configuration

    const configPath = getConfigPathFromCLI(parseCLIOptions());
    const config = parseConfigFile(configPath);

    logSuccess("Configuration file is valid");

    // Find localization files

    const files = await getL10nFiles(config);

    if (files.length === 0) {
      logWarning("No localization files were detected");
    }

    // Get and validate messages

    const messages = await readAllL10nFiles(files, config);

    if (messages.length === 0) {
      logWarning("No messages were detected");
    }

    const messageMap = createMessageMap(messages);

    logSuccess("Messages were parsed successfully");

    // Get storage information

    const storageManifest = await getStorageManifest();

    logSuccess("Storage manifest was read successfully");

    // Generate diff

    const diff = await getLastVersionDiff(messageMap, storageManifest);

    // Write master file

    await writeNewVersionInStorage(messageMap, storageManifest);

    logSuccess("Master file was successfully written");

    // Print report

    logVersionDiffReport(diff);
  } catch (e) {
    logError(e);
  }
})();
