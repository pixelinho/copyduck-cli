import { parseCLIOptions } from "./cliParsers";
import * as processGetters from "../process/processGetters";

describe("parseCLIOptions", () => {
  const execPath = ".../bin/node";
  const file = ".../node_modules/.bin/name";
  const key1 = "a";
  const key2 = "b";
  const opt1 = `--${key1}`;
  const opt2 = `--${key2}`;

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("parses options", () => {
    const val1 = "c";
    const val2 = "d";

    jest
      .spyOn(processGetters, "getCLIArguments")
      .mockReturnValue([execPath, file, opt1, val1, opt2, val2]);

    expect(parseCLIOptions()).toEqual({
      [key1]: val1,
      [key2]: val2
    });
  });

  it("doesn't parse invalid options", () => {
    jest
      .spyOn(processGetters, "getCLIArguments")
      .mockReturnValue([execPath, file, "invalid", "--invalid0", "--INVALID"]);

    expect(parseCLIOptions()).toEqual({});
  });

  it("doesn't parse the executable path and the executed file", () => {
    jest.spyOn(processGetters, "getCLIArguments").mockReturnValue([opt1, opt2]);

    expect(parseCLIOptions()).toEqual({});
  });

  it("casts a value to a number if the option value is a number", () => {
    const val1 = 1;

    jest
      .spyOn(processGetters, "getCLIArguments")
      .mockReturnValue([execPath, file, opt1, `${val1}`]);

    expect(parseCLIOptions()).toEqual({
      [key1]: val1
    });
  });

  it("casts a value to a number if the option value is a boolean or not defined", () => {
    const val1 = false;

    jest
      .spyOn(processGetters, "getCLIArguments")
      .mockReturnValue([execPath, file, opt1, `${val1}`, opt2]);

    expect(parseCLIOptions()).toEqual({
      [key1]: val1,
      [key2]: true
    });
  });
});
