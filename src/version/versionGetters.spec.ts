import {
  getMostRecentVersion,
  getVersionTimestamp,
  getVersionFilename
} from "./versionGetters";

describe("getMostRecentVersion", () => {
  const filename = "file.json";

  it("gets the most recent version of a storage manifest", () => {
    const version1 = { timestamp: 1, filename };
    const version2 = { timestamp: 2, filename };
    const version3 = { timestamp: 3, filename };

    expect(
      getMostRecentVersion({ versions: [version1, version2, version3] })
    ).toEqual(version3);
  });

  it("returns undefined if the storage manifest has no version", () => {
    expect(getMostRecentVersion({ versions: [] })).toBeUndefined();
  });
});

describe("getVersionTimestamp", () => {
  it("returns the timestamp", () => {
    const timestamp = 1;
    const version = { timestamp, filename: "file.json" };

    expect(getVersionTimestamp(version)).toEqual(timestamp);
  });
});

describe("getVersionFilename", () => {
  it("returns the filename", () => {
    const filename = "file.json";
    const version = { timestamp: 1, filename };

    expect(getVersionFilename(version)).toEqual(filename);
  });
});
