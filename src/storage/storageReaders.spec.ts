import { promises as fsPromises } from "fs";
import { readStorageManifest } from "./storageReaders";
import { storageManifestFilename } from "./storageSettings";

describe("readStorageManifest", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("reads the storage manifest file", async () => {
    const content = { versions: [] };
    const spy = jest
      .spyOn(fsPromises, "readFile")
      .mockResolvedValue(JSON.stringify(content));
    const manifest = await readStorageManifest();

    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(storageManifestFilename),
      "utf8"
    );
    expect(manifest).toEqual(content);
  });
});
