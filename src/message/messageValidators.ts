import * as chalk from "chalk";

/**
 * Checks if a message is valid
 * @param {*} message Message to validate
 * @returns {string[]} Reasons why it may be invalid
 */
export function validateMessage(message: any): string[] {
  const errors = [];

  if (typeof message !== "object" || message === null) {
    errors.push("message must be an object");
  } else {
    const { id, text } = message;

    if (typeof id === "undefined") {
      errors.push("message has no id");
    } else {
      errors.push(...validateMessageId(message.id));
    }

    if (typeof text === "undefined") {
      errors.push("message has no text");
    } else {
      errors.push(...validateMessageText(message.text));
    }
  }

  return errors.map(e => `${e} ${chalk`{dim ${JSON.stringify(message)}}`}`);
}

/**
 * Checks if the id of a message is valid
 * @param {*} id Id to validate
 * @returns {string[]} Reasons why it may be invalid
 */
export function validateMessageId(id: any): string[] {
  const errors = [];

  if (typeof id !== "string") {
    errors.push("id is not a string");
  }

  if (id === "") {
    errors.push("id is empty");
  }

  return errors;
}

/**
 * Checks if the text of a message is valid
 * @param {*} text Text to validate
 * @returns {string[]} Reasons why it may be invalid
 */
export function validateMessageText(text: any): string[] {
  const errors = [];

  if (typeof text !== "string") {
    errors.push("text is not a string");
  }

  if (text === "") {
    errors.push("text is empty");
  }

  return errors;
}
