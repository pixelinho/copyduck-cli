import { getCurrentWorkingDirectory, getCLIArguments } from "./processGetters";

describe("getCurrentWorkingDirectory", () => {
  it("returns a string", () => {
    expect(getCurrentWorkingDirectory()).toEqual(expect.any(String));
  });
});

describe("getCLIArguments", () => {
  it("returns an array", () => {
    expect(Array.isArray(getCLIArguments())).toEqual(true);
  });
});
