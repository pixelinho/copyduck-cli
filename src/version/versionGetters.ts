import { getStorageVersions } from "../storage/storageGetters";
import { StorageManifest } from "../storage/storageTypes";
import { sortVersions } from "./versionHelpers";
import { Version } from "./versionTypes";

/**
 * Get the most recent version of a storage manifest
 * @param {StorageManifest} storageManifest Storage manifest
 * @returns {Version} Most recent version
 */
export function getMostRecentVersion(
  storageManifest: StorageManifest
): Version {
  const versions = getStorageVersions(storageManifest);

  versions.sort(sortVersions);

  return versions[0];
}

/**
 * Returns the timestamp of a version
 * @param {Version} version Version of which to return the timestamp
 * @returns {number} Timestamp
 */
export function getVersionTimestamp(version: Version): number {
  return version.timestamp;
}

/**
 * Returns the filename of a version
 * @param {Version} version Version of which to return the filename
 * @returns {string} Filename
 */
export function getVersionFilename(version: Version): string {
  return version.filename;
}
