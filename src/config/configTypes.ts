import { Message } from "../message/messageTypes";

export interface Config {
  /**
   * The pattern used to detect localization files
   */
  l10nRegex: RegExp;
  /**
   * Paths of the directories in which to look for localization files
   */
  include: string[];
  /**
   * Function used to extract messages from a localization file
   */
  messageMapper: (fileContent: string) => Message[];
}
