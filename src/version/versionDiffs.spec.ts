import { createVersionDiff, getLastVersionDiff } from "./versionDiffs";
import * as versionReaders from "./versionReaders";

describe("getLastVersionDiff", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("returns a version difference", async () => {
    jest.spyOn(versionReaders, "readVersionFile").mockResolvedValue({});

    expect(await getLastVersionDiff({}, { versions: [] } as any)).toEqual({
      removed: [],
      updated: [],
      created: []
    });
  });
});

describe("createVersionDiff", () => {
  const id = "1";
  const text = "abc";

  it("adds removed messages to the diff", () => {
    const { removed, updated, created } = createVersionDiff({}, {
      [id]: text
    } as any);

    expect(removed).toEqual([{ id, text }]);
    expect(updated).toEqual([]);
    expect(created).toEqual([]);
  });

  it("adds created messages to the diff", () => {
    const { removed, updated, created } = createVersionDiff(
      { [id]: text } as any,
      {}
    );

    expect(removed).toEqual([]);
    expect(updated).toEqual([]);
    expect(created).toEqual([{ id, text }]);
  });

  it("adds updated messages to the diff", () => {
    const text2 = "def";

    const { removed, updated, created } = createVersionDiff(
      { [id]: text } as any,
      { [id]: text2 } as any
    );

    expect(removed).toEqual([]);
    expect(updated).toEqual([
      {
        current: {
          id,
          text
        },
        previous: {
          id,
          text: text2
        }
      }
    ]);
    expect(created).toEqual([]);
  });
});
