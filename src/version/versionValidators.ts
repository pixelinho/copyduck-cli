/**
 * Checks if a version is valid
 * @param {*} value Version to validate
 * @returns {string[]} Reasons why it may be invalid
 */
export function validateVersion(value: any): string[] {
  const errors = [];

  if (typeof value !== "object" || value === null) {
    errors.push("version must be an object");
  } else {
    if (!("timestamp" in value)) {
      errors.push("version must have a `timestamp` key");
    } else if (typeof value.timestamp !== "number") {
      errors.push("timestamp must be a number");
    }

    if (!("filename" in value)) {
      errors.push("version must have a `filename` key");
    } else if (typeof value.filename !== "string") {
      errors.push("filename must be a string");
    }
  }

  return errors;
}
