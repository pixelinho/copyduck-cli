import { promises as fsPromises } from "fs";
import { Config } from "../config/configTypes";
import { Message } from "../message/messageTypes";
import { validateMessage } from "../message/messageValidators";
import L10nParsingError from "./error/L10nParsingError";
import { composeException } from "../log/logHelpers";

/**
 * Reads a localization file and extracts messages from it
 * @param {string} file Path to the localization file
 * @param {Config} config Configuration object
 * @returns {Promise<Messages[]>} A promise that resolves with the messages
 * extract from the file
 *
 * @throws Will throw an exception if the file can't be read, if the mapping
 * function throws an exception or if any message is invalid
 */
export async function readL10nFile(
  file: string,
  { messageMapper }: Config
): Promise<Message[]> {
  let messages = [];

  try {
    messages = messageMapper(await fsPromises.readFile(file, "utf8"));
  } catch (e) {
    throw composeException(new L10nParsingError(file, "An error occured"), e);
  }

  if (!Array.isArray(messages)) {
    throw new L10nParsingError(file, "`messageMapper` must return an array");
  }

  const errors = messages.reduce(
    (acc, message) => [...acc, ...validateMessage(message)],
    []
  );
  const { length: errorCount } = errors;

  if (errorCount > 0) {
    throw new L10nParsingError(file, `${errorCount} error(s) occured`, errors);
  }

  return messages;
}

/**
 * Reads all localization files and extracts messages
 * @param {string} files Paths to the localization files
 * @param {Config} config Configuration object
 * @returns {Promise<Messages[]>} A promise that resolves with the extracted
 * messages
 *
 * @throws Will throw an exception if any of localization file could not
 * be parsed
 */
export async function readAllL10nFiles(
  files: string[],
  config: Config
): Promise<Message[]> {
  const promises = files.map(file => readL10nFile(file, config).catch(e => e));

  return Promise.all(promises).then(results => {
    const errors = results.filter(item => item instanceof Error);

    if (errors.length > 0) {
      throw new Error(`Could not parse all localization files

${errors.map(e => e.message).join("\n\n")}`);
    }

    return results.reduce((acc, messages) => [...acc, ...messages], []);
  });
}
