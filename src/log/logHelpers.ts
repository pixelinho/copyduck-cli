import * as chalk from "chalk";

/**
 * Appends and format an exception stack trace to the message of
 * another exception
 * @param {Error} e1 Exception of which the message is augmented
 * @param {Error} e2 Exception of which the stack trace is appended
 * @returns {Error} Updated exception
 */
export function composeException(e1: Error, e2: Error): Error {
  e1.message = chalk`${e1.message}
{dim ${e2.stack}}`;

  return e1;
}
