/* tslint:disable:no-console */

import { red, green, yellow, cyan } from "chalk";

/**
 * Outputs a success message
 * @param {*} value Value to output
 */
export function logSuccess(value: any): void {
  console.log(green(`\u2714 ${value}`));
}

/**
 * Outputs an information message
 * @param {*} value Value to output
 */
export function logInfo(value: any): void {
  console.log(cyan(`${value}`));
}

/**
 * Outputs a warning message
 * @param {*} value Value to output
 */
export function logWarning(value: any): void {
  console.log(yellow(`Warning: ${value}`));
}

/**
 * Outputs an error message
 * @param {*} value Value to output
 */
export function logError(value: any): void {
  console.error(red(value));
}
