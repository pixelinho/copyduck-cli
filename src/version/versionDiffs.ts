import { MessageMap } from "../message/messageTypes";
import { StorageManifest } from "../storage/storageTypes";
import { getMostRecentVersion } from "./versionGetters";
import { readVersionFile } from "./versionReaders";
import { VersionDiff } from "./versionTypes";

/**
 * Get the version difference between a message map and the last version stored
 * @param {MessageMap} messageMap Message map used for comparison
 * @param {StorageManifest} storageManifest Storage manifest
 * @param {string} cwd Directory from which the script is run
 * @returns {Promise<VersionDiff>} A promise that resolves with the version
 * difference
 */
export async function getLastVersionDiff(
  messageMap: MessageMap,
  storageManifest: StorageManifest
): Promise<VersionDiff> {
  const mostRecentVersion = getMostRecentVersion(storageManifest);
  const prevMessageMap = mostRecentVersion
    ? await readVersionFile(mostRecentVersion)
    : {};

  return createVersionDiff(messageMap, prevMessageMap);
}

/**
 * Create a version difference from two message maps
 * @param {MessageMap} messageMap Latest message map
 * @param {MessageMap} prevMessageMap Previous message map
 * @returns {VersionDiff} Version difference
 */
export function createVersionDiff(
  messageMap: MessageMap,
  prevMessageMap: MessageMap
): VersionDiff {
  const removed = [];
  const created = [];
  const updated = [];

  for (const [id, text] of Object.entries(messageMap)) {
    if (!(id in prevMessageMap)) {
      created.push({ id, text });
    } else if (messageMap[id] !== prevMessageMap[id]) {
      updated.push({
        current: {
          id,
          text: messageMap[id]
        },
        previous: {
          id,
          text: prevMessageMap[id]
        }
      });
    }
  }

  for (const [id, text] of Object.entries(prevMessageMap)) {
    if (!(id in messageMap)) {
      removed.push({ id, text });
    }
  }

  return {
    removed,
    created,
    updated
  };
}
