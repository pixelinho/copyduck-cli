import { CLIOptions } from "../cli/cliTypes";
import { getConfigPathFromCLI } from "./configHelpers";
import { configOption, defaultConfigPath } from "./configSettings";
import { getCurrentWorkingDirectory } from "../process/processGetters";

describe("getConfigPathFromCLI", () => {
  const configPath = "config/config.js";

  it("uses the path from the CLI options", () => {
    expect(
      getConfigPathFromCLI({ [configOption]: configPath } as CLIOptions)
    ).toEqual(`${getCurrentWorkingDirectory()}/${configPath}`);

    expect(
      getConfigPathFromCLI({
        [configOption]: `./${configPath}`
      } as CLIOptions)
    ).toEqual(`${getCurrentWorkingDirectory()}/${configPath}`);
  });

  it("uses the default path if no config option is specified", () => {
    expect(getConfigPathFromCLI()).toEqual(
      `${getCurrentWorkingDirectory()}/${defaultConfigPath}`
    );
  });
});
