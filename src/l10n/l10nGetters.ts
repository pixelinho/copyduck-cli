import { Config } from "../config/configTypes";
import { walkDir } from "../fs/fsHelpers";

/**
 * Gets localization files from the configuration settings
 * @param {Config} config Configuration
 * @returns {Promise<string[]>} A promise that resolves with the paths to the
 * localization files
 */
export async function getL10nFiles({
  l10nRegex,
  include
}: Config): Promise<string[]> {
  const result = [];

  await Promise.all(
    include.map(dirPath =>
      walkDir(filePath => {
        if (l10nRegex.test(filePath)) {
          result.push(filePath);
        }
      }, dirPath)
    )
  );

  return result;
}
