import { promises as fsPromises } from "fs";
import { getStoragePath, joinStoragePath } from "./storageHelpers";
import { storageManifestFilename } from "./storageSettings";
import { StorageManifest } from "./storageTypes";
import { MessageMap } from "../message/messageTypes";
import { versionBasename } from "../version/versionSettings";
import { pushVersionToStorageManifest } from "./storageSetters";
import { writeVersionFile } from "../version/versionWriters";
import { composeException } from "../log/logHelpers";

/**
 * Creates the storage directory
 * @returns {Promse<void>} A promise that resolves when the directory
 * is created
 */
export async function makeStorageDirectory(): Promise<void> {
  return fsPromises.mkdir(getStoragePath());
}

/**
 * Writes the storage manifest on the file system
 * @param {StorageManifest} storageManifest
 * @returns {Promse<void>} A promise that resolves when the manifest is written
 */
export async function writeStorageManifest(
  storageManifest: StorageManifest
): Promise<void> {
  await fsPromises.writeFile(
    joinStoragePath(storageManifestFilename),
    JSON.stringify(storageManifest, null, 2)
  );
}

/**
 * Writes a new version in the storage directory and updates the manifest
 * @param {MessageMap} messageMap Content of the new version
 * @param {StorageManifest} storageManifest Storage manifest
 * @returns {Promise<void>} A promise that resolves when the version is written
 * and the manifest is updated
 */
export async function writeNewVersionInStorage(
  messageMap: MessageMap,
  storageManifest: StorageManifest
): Promise<void> {
  // Create storage directory if it doesn't exit yet
  try {
    await fsPromises.readdir(getStoragePath());
  } catch (e) {
    if ("ENOENT" !== e.code) {
      throw composeException(new Error("Could not read storage directory"), e);
    }

    await makeStorageDirectory();
  }

  const version = await writeVersionFile(messageMap);

  await pushVersionToStorageManifest(version, storageManifest);
  await writeStorageManifest(storageManifest);
}
