import { Version } from "../version/versionTypes";

export interface StorageManifest {
  /**
   * Stored versions
   */
  versions: Version[];
}
