export interface Message {
  /**
   * Unique identifier of the message
   */
  id: string;
  /**
   * Text of the message
   */
  text: string;
}

export interface MessageMap {
  /**
   * Id/text pair
   */
  [id: string]: [string];
}
