import { promises as fsPromises } from "fs";
import { getStorageManifest, getStorageVersions } from "./storageGetters";

describe("getStorageManifest", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("returns the storage manifest", async () => {
    const manifest = { versions: [{ timestamp: 1, filename: "file.json" }] };

    jest
      .spyOn(fsPromises, "readFile")
      .mockResolvedValue(JSON.stringify(manifest));

    expect(await getStorageManifest()).toEqual(manifest);
  });

  it("returns a new storage manifest if it doesn't exist yet", async () => {
    jest.spyOn(fsPromises, "readFile").mockRejectedValue({ code: "ENOENT" });

    expect(await getStorageManifest()).toEqual({ versions: [] });
  });

  it("throws an exception if the file can't be read", () => {
    const e = new Error();

    jest.spyOn(fsPromises, "readFile").mockRejectedValue(e);

    return expect(getStorageManifest()).rejects.toThrow();
  });

  it("throws an exception if the storage manifest is invalid", () => {
    jest
      .spyOn(fsPromises, "readFile")
      .mockResolvedValue({ _versions: [] } as any);

    return expect(getStorageManifest()).rejects.toThrow();
  });
});

describe("getStorageVersions", () => {
  it("returns the versions", () => {
    const version = { timestamp: 1, filename: "file.json" };

    expect(getStorageVersions({ versions: [version] })).toEqual([version]);
  });
});
