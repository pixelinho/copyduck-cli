import { createMessageMap } from "./messageMap";

const id1 = "123";
const id2 = "456";
const text1 = "abc";
const text2 = "def";
const text3 = "ghi";
const message1 = { id: id1, text: text1 };
const message2 = { id: id2, text: text2 };
const message3 = { id: id1, text: text3 };

describe("createMessageMap", () => {
  it("throws an exception if ids are not unique", () => {
    expect(() => createMessageMap([message1, message2, message3])).toThrow(
      "unique"
    );
  });

  it("returns a map", () => {
    expect(createMessageMap([message1, message2])).toEqual({
      [id1]: text1,
      [id2]: text2
    });
  });
});
