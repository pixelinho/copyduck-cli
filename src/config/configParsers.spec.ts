import {
  parseConfigFile,
  parseInclude,
  parseL10nRegex,
  parseMessageMapper
} from "./configParsers";
import * as configReader from "./configReader";
import { getCurrentWorkingDirectory } from "../process/processGetters";

describe("parseConfigFile", () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it("returns a configuration object", () => {
    const config = {
      l10nRegex: /\.l10n\.json$/,
      include: ["src"],
      messageMapper: () => null
    };

    jest.spyOn(configReader, "default").mockReturnValue(config);

    expect(parseConfigFile("/path")).toEqual(
      expect.objectContaining({
        l10nRegex: expect.any(RegExp),
        include: expect.any(Array),
        messageMapper: expect.any(Function)
      })
    );
  });
});

describe("parseL10nRegex", () => {
  it("throws an exception if the argument is neither a string nor a RegExp", () => {
    expect(() => parseL10nRegex(1 as any)).toThrow(TypeError);
  });

  it("returns the argument if it is a RegExp", () => {
    const l10nRegex = /\.l10n\.json$/;

    expect(parseL10nRegex(l10nRegex)).toBe(l10nRegex);
  });

  it("returns a RegExp if the argument is a string", () => {
    const l10nRegex = ".l10n.json$";

    expect(parseL10nRegex(l10nRegex)).toEqual(expect.any(RegExp));
  });

  it("throws an exception if the argument is a string that generates an invalid RegExp", () => {
    // TODO: write test
  });
});

describe("parseInclude", () => {
  it("throws an exception if the value is neither a string nor an array", () => {
    expect(() => parseInclude(1 as any)).toThrow(TypeError);
  });

  it("returns the path relative to the current working directory if the value is a string", () => {
    const include = "src";

    expect(parseInclude(include)).toEqual([
      `${getCurrentWorkingDirectory()}/${include}`
    ]);
  });

  it("returns the path relative to the current working directory if the vlaue is an array", () => {
    const include = ["src"];

    expect(parseInclude(include)).toEqual([
      `${getCurrentWorkingDirectory()}/${include}`
    ]);
  });

  it("throws an exception if the value is an array that doesn't only contain strings", () => {
    const include = ["src", 1];

    expect(() => parseInclude(include)).toThrow(TypeError);
  });
});

describe("parseMessageMapper", () => {
  it("throws an exception if the argument is not a function", () => {
    expect(() => parseMessageMapper(1 as any)).toThrow(TypeError);
  });

  it("returns the argument", () => {
    const fn = () => null;

    expect(parseMessageMapper(fn)).toBe(fn);
  });
});
