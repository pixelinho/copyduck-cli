import { promises as fsPromises } from "fs";
import { versionBasename } from "./versionSettings";
import { deleteVersionFile, writeVersionFile } from "./versionWriters";

describe("writeVersionFile", () => {
  const messageMap = { home: "Home" };
  const timestamp = 123;

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("writes to the file system", async () => {
    const spy = jest.spyOn(fsPromises, "writeFile").mockResolvedValue();

    jest.spyOn(Date, "now").mockReturnValue(timestamp);

    await writeVersionFile(messageMap as any);

    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(versionBasename),
      expect.any(String)
    );
    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(`${timestamp}`),
      expect.any(String)
    );
  });

  it("resolves with the new version", async () => {
    jest.spyOn(fsPromises, "writeFile").mockResolvedValue();

    const version = await writeVersionFile(messageMap as any);

    expect(version).toHaveProperty("timestamp");
    expect(version).toHaveProperty("filename");
  });
});

describe("deleteVersionFile", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("deletes the version file", () => {
    const spy = jest.spyOn(fsPromises, "unlink").mockResolvedValue();
    const filename = "file.json";

    deleteVersionFile({ timestamp: 123, filename });

    expect(spy).toHaveBeenCalledWith(expect.stringContaining(filename));
  });
});
