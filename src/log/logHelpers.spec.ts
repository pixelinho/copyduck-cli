import { composeException } from "./logHelpers";

describe("composeException", () => {
  it("composes the exception", () => {
    const msg1 = "Invalid syntax";
    const msg2 = "File doesn't exist";
    const e1 = new Error(msg1);
    const e2 = new Error(msg2);

    composeException(e1, e2);

    const { message } = e1;

    expect(message).toEqual(expect.stringContaining(msg1));
    expect(message).toEqual(expect.stringContaining(msg2));
  });
});
