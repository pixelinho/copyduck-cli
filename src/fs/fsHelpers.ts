import { promises as fsPromises } from "fs";
import { join } from "path";

/**
 * Checks if the path is the path of a directory
 * @param {string} path Path to check
 * @returns {Promise<boolean>} Promise that resolves to true if it is
 */
export async function isPathDirectory(path: string): Promise<boolean> {
  return fsPromises.lstat(path).then(stats => stats.isDirectory());
}

/**
 * Parses a directoy recursively and calls the callback whenever it encounters
 * a file. The callback is called with the file path.
 * @callback fileCallback
 * @param {string} Path to the file
 *
 * @param {fileCallback} cb Callback called when the function encounters a file
 * @param {string} dirPath Directory to parse
 * @returns {Promise<void>} A promise that resolves when the directory and its
 * subdirectories are parsed
 */
export async function walkDir(
  cb: (path: string) => void,
  dirPath: string
): Promise<void> {
  if (await isPathDirectory(dirPath)) {
    const paths = await fsPromises.readdir(dirPath);
    const promises = paths.map(async path => {
      path = join(dirPath, path);

      if (await isPathDirectory(path)) {
        return walkDir(cb, path);
      }

      cb(path);
    });

    return Promise.all(promises).then(() => null);
  }

  return Promise.resolve();
}
