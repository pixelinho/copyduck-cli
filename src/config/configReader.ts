import { composeException } from "../log/logHelpers";

/**
 * Reads the configuration file synchronously as a Javascript module, and
 * returns the object the module exports
 * @param {string} path Path to the configuration file
 * @returns {Object} Configuration object
 *
 * @throws Will throw an exception if the file can't be read or if the module
 * doesn't export an object
 */
export default function readConfigFile(path: string): object {
  let data = {};

  try {
    data = require(path);
  } catch (e) {
    throw composeException(
      new Error(`Could not open configuration file ${path}`),
      e
    );
  }

  if (typeof data !== "object") {
    throw new Error("Configuration must be defined as an object");
  }

  return data;
}
