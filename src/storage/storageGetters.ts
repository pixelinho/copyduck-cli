import { Version } from "../version/versionTypes";
import { readStorageManifest } from "./storageReaders";
import { StorageManifest } from "./storageTypes";
import { validateStorageManifest } from "./storageValidators";
import { composeException } from "../log/logHelpers";

/**
 * Gets the storage manifest of the project
 * @returns {Promise<StorageManifest>} Storage manifest
 *
 * @throws Will throw an exception if the storage manifest file can't be read
 * or if the manifest is invalid
 */
export async function getStorageManifest(): Promise<StorageManifest> {
  let data;

  try {
    data = await readStorageManifest();
  } catch (e) {
    if ("ENOENT" !== e.code) {
      throw composeException(
        new Error("Could not read storage manifest file"),
        e
      );
    }

    data = { versions: [] };
  }

  const errors = validateStorageManifest(data);
  const { length: errorCount } = errors;

  if (errorCount > 0) {
    throw new Error(`${errorCount} error(s) occured
${errors.map(e => `  - ${e}`).join("\n")}`);
  }

  return data;
}

/**
 * Returns the versions of a storage manifest
 * @param {StorageManifest} storageManifest Storage manifest of which to return
 * the versions
 * @returns {Version[]} Versions
 */
export function getStorageVersions(
  storageManifest: StorageManifest
): Version[] {
  return storageManifest.versions;
}
