import { join } from "path";
import { getCurrentWorkingDirectory } from "../process/processGetters";
import { storageDir, storageManifestFilename } from "./storageSettings";

/**
 * Returns the path to the storage directory
 * @returns {string} Path
 */
export function getStoragePath(): string {
  return join(getCurrentWorkingDirectory(), storageDir);
}

/**
 * Returns the path to the storage manifest file
 * @returns {string} Path
 */
export function getStorageManifestPath(): string {
  return join(getStoragePath(), storageManifestFilename);
}

/**
 * Joins the storage directory and the path passed as argument
 * @returns {string} Joined path
 */
export function joinStoragePath(path: string): string {
  return join(getStoragePath(), path);
}
