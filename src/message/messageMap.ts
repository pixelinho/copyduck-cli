import { Message, MessageMap } from "./messageTypes";

/**
 * Creates an object of which keys are message ids and values message texts
 * @param {Message[]} messages List of messages
 * @returns {MessageMap} Message map
 *
 * @throws Will throw an exception if message ids are not unique
 */
export function createMessageMap(messages: Message[]): MessageMap {
  const map = {};
  const duplicates = [];

  messages.forEach(({ id, text }) => {
    if (id in map) {
      duplicates.push(id);
    } else {
      map[id] = text;
    }
  });

  if (duplicates.length > 0) {
    throw new Error(`Message ids must be unique. Duplicates found:
${duplicates.join("\n")}`);
  }

  return map;
}
