import { promises as fsPromises } from "fs";
import {
  makeStorageDirectory,
  writeStorageManifest,
  writeNewVersionInStorage
} from "./storageWriters";
import { storageDir, storageManifestFilename } from "./storageSettings";
import * as versionWriters from "../version/versionWriters";
import { getStorageVersions } from "./storageGetters";

describe("makeStorageDirectory", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("creates a directory", async () => {
    const spy = jest.spyOn(fsPromises, "mkdir").mockResolvedValue();

    await makeStorageDirectory();

    expect(spy).toHaveBeenCalledWith(expect.stringContaining(storageDir));
  });
});

describe("writeStorageManifest", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("writes the file", async () => {
    const spy = jest.spyOn(fsPromises, "writeFile").mockResolvedValue();

    await writeStorageManifest({ versions: [] });

    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(storageManifestFilename),
      expect.any(String)
    );
  });
});

describe("writeNewVersionInStorage", () => {
  const messageMap = { home: "Home" };
  const timestamp = 123;
  const filename = "file.json";
  const version = { timestamp, filename };

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("throws an exception if the storage directory can't be read", () => {
    jest.spyOn(fsPromises, "readdir").mockRejectedValue(new Error());

    return expect(
      writeNewVersionInStorage(messageMap as any, { versions: [] })
    ).rejects.toThrow();
  });

  it("makes the storage directory if it doesn't exist yet", async () => {
    const spy = jest.spyOn(fsPromises, "mkdir").mockResolvedValue();

    jest.spyOn(fsPromises, "readdir").mockRejectedValue({ code: "ENOENT" });

    await writeNewVersionInStorage(messageMap as any, { versions: [] });

    expect(spy).toHaveBeenCalled();
  });

  it("updates the storage manifest", async () => {
    const storageManifest = { versions: [] };

    jest.spyOn(fsPromises, "writeFile").mockResolvedValue();
    jest.spyOn(fsPromises, "readdir").mockResolvedValue([]);

    await writeNewVersionInStorage(messageMap as any, storageManifest);

    expect(getStorageVersions(storageManifest)).toHaveLength(1);
  });

  it("writes the version file and storage manifest", async () => {
    const spy = jest.spyOn(fsPromises, "writeFile").mockResolvedValue();

    jest.spyOn(fsPromises, "readdir").mockResolvedValue([]);

    await writeNewVersionInStorage(messageMap as any, { versions: [] });

    expect(spy).toHaveBeenCalledTimes(2);
  });
});
