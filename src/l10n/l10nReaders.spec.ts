import { promises as fsPromises } from "fs";
import { readL10nFile, readAllL10nFiles } from "./l10nReaders";
import { Config } from "../config/configTypes";
import L10nParsingError from "./error/L10nParsingError";

describe("readL10nFile", () => {
  const file = "./file.l10n.json";
  const id = "123";
  const text = "abc";
  const validMessage = { id, text };
  const invalidMessage = { id: "", text };
  const messageMapper = arg => [arg];

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("throws an exception if the file can't be read", () => {
    const errorMessage = "custom error message";

    jest
      .spyOn(fsPromises, "readFile")
      .mockRejectedValue(new Error(errorMessage));

    return expect(
      readL10nFile(file, { messageMapper } as Config)
    ).rejects.toThrow(errorMessage);
  });

  it("throws an exception if the parsing of the file fails", () => {
    jest.spyOn(fsPromises, "readFile").mockResolvedValue(validMessage as any);

    const errorMessage = "custom error message";

    return expect(
      readL10nFile(file, {
        messageMapper: () => {
          throw new Error(errorMessage);
        }
      } as any)
    ).rejects.toThrow(errorMessage);
  });

  it("throws an exception if the parsing of the file doesn't return an array", () => {
    jest.spyOn(fsPromises, "readFile").mockResolvedValue(validMessage as any);

    return expect(
      readL10nFile(file, { messageMapper: arg => 1 } as any)
    ).rejects.toThrow("array");
  });

  it("throws an exception if the parsing of the file generates invalid messages", () => {
    jest.spyOn(fsPromises, "readFile").mockResolvedValue(invalidMessage as any);

    return expect(readL10nFile(file, { messageMapper } as any)).rejects.toThrow(
      text
    );
  });

  it("reads the content of the file and map the messages", async () => {
    jest.spyOn(fsPromises, "readFile").mockResolvedValue(validMessage as any);

    const messages = await readL10nFile(file, { messageMapper } as Config);

    expect(messages).toHaveLength(1);
  });
});

describe("readAllL10nFiles", () => {
  const files = ["./file1.l10n.json", "./file2.l10n.json"];
  const id1 = "123";
  const text1 = "abc";
  const validMessage1 = { id: id1, text: text1 };
  const id2 = "456";
  const text2 = "def";
  const validMessage2 = { id: id2, text: text2 };
  const invalidMessage = { id: "", text: text1 };
  const messageMapper = arg => [arg];

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("throws an exception if one of the messages is invalid", () => {
    jest
      .spyOn(fsPromises, "readFile")
      .mockRejectedValue(invalidMessage as any)
      .mockResolvedValueOnce(validMessage1 as any);

    return expect(
      readAllL10nFiles(files, { messageMapper } as Config)
    ).rejects.toThrow("parse");
  });

  it("reads the content of all files and map the messages", async () => {
    jest
      .spyOn(fsPromises, "readFile")
      .mockResolvedValue(validMessage2 as any)
      .mockResolvedValueOnce(validMessage1 as any);

    const messages = await readAllL10nFiles(files, {
      messageMapper
    } as any);

    expect(messages).toHaveLength(2);
  });
});
