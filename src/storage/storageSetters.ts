import { clearVersions } from "../version/versionHelpers";
import { Version } from "../version/versionTypes";
import { getStorageVersions } from "./storageGetters";
import { StorageManifest } from "./storageTypes";

/**
 * Adds a version to the storage manifest and clear older versions if the
 * storage limit is reached
 * @param {Version} version Version to add
 * @param {StorageManifest} storageManifest Storage manifest
 * @returns {Promise<void>} A promise that resolves when the version id added
 * and the older versions removed
 */
export async function pushVersionToStorageManifest(
  version: Version,
  storageManifest: StorageManifest
): Promise<void> {
  getStorageVersions(storageManifest).push(version);

  await clearVersions(storageManifest);
}
