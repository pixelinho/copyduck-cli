import {
  validateMessage,
  validateMessageId,
  validateMessageText
} from "./messageValidators";

describe("validateMessage", () => {
  it("returns an error message if the message is not an object", () => {
    const expected = [expect.stringContaining("object")];

    expect(validateMessage(null)).toEqual(expected);
    expect(validateMessage(1)).toEqual(expected);
  });

  it("returns an error message if the message has no id", () => {
    expect(validateMessage({ text: "text" })).toEqual([
      expect.stringContaining("no id")
    ]);
  });

  it("returns an error message if the message has no text", () => {
    expect(validateMessage({ id: "id" })).toEqual([
      expect.stringContaining("no text")
    ]);
  });

  it("returns error messages if id and text are invalid", () => {
    expect(validateMessage({ id: 1, text: "" })).toHaveLength(2);
  });

  it("prepends the message to the errors", () => {
    const text = "lorem";

    expect(validateMessage({ id: "", text })).toEqual([
      expect.stringContaining(text)
    ]);
  });

  it("returns an empty array if the message is valid", () => {
    expect(validateMessage({ id: "id", text: "text" })).toEqual([]);
  });
});

describe("validateMessageId", () => {
  it("returns an error message if the id is not a string", () => {
    expect(validateMessageId(1)).toEqual([expect.stringContaining("string")]);
  });

  it("returns an error message if the id is empty", () => {
    expect(validateMessageId("")).toEqual([expect.stringContaining("empty")]);
  });

  it("returns an empty array if the id is valid", () => {
    expect(validateMessageId("a")).toEqual([]);
  });
});

describe("validateMessageText", () => {
  it("returns an error message if the text is not a string", () => {
    expect(validateMessageText(1)).toEqual([expect.stringContaining("string")]);
  });

  it("returns an error message if the text is empty", () => {
    expect(validateMessageText("")).toEqual([expect.stringContaining("empty")]);
  });

  it("returns an empty array if the text is valid", () => {
    expect(validateMessageText("a")).toEqual([]);
  });
});
