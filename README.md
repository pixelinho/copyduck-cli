# Copyduck (work-in-progress)

Hand copy updates over to your translators and integrate their translations.

## Installation

Make sure your system has [Node](http://nodejs.org) installed.

### Node

This package should be working with version 12 or higher. To check the version installed on your machine, bring up a terminal and type `node --version`. If you require Node, go to [nodeJS.org](http://nodejs.org/) and click on the big green Install button.

### Local Dependencies

#### Installation

Next, install the local dependencies the project requires:

```sh
$ npm install
```

That’s it! You should now have everything needed to run the project.

## Usage

Copyduck provides 2 scripts.

One to generate the files with the missing translations, that will be handed over to the translators:

```sh
$ copyduck-generate
```

And another one to import the translations in the project:

```sh
$ copyduck-import
```

### Configuration

#### Configuration file

Both scripts share a single configuration file. By default, Copyduck will check if there's a file named `copyduck.config.js` in the directory from which you run the script, usually the root of your project. You can override this behaviour by specifying the path to the configuration file with the `config` option, e.g.:

```sh
$ copyduck-generate --config i18n.config.js
```

The configuration file **must** be a CommonJS module that exports a configuration object:

    module.exports = {
      l10nRegex: /\.l10n\.json$/i,
      include: ['src'],
      ...
    };

#### Options

| Option          | Description                                                                                                                                                                                                                                     | Type                   | Required | Default Value |
| --------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- | :------: | :-----------: |
| `l10nRegex`     | The pattern used to detect localization files                                                                                                                                                                                                   | `string` or `RegExp`   |   Yes    |      n/a      |
| `include`       | Paths of the directories in which to look for localization files                                                                                                                                                                                | `string` or `string[]` |   Yes    |      n/a      |
| `messageMapper` | Function used to extract messages from a localization file. The function takes the content of a localization file as a string, and must return an array of messages. A message is an object with the signature: `{ id: string, text: string }`. | `function`             |   Yes    |      n/a      |

#### Example

To parse the texts from a localization file of which the path is `src/nav.l10n.json` and the content is:

    {
      "home": {
        "id": "nav.home.label",
        "defaultMessage": "Home"
      }
    }

The configuration file would look like:

    module.exports = {
      l10nRegex: /\.l10n\.json$/i,
      include: ['src'],
      messageMapper: fileContent => {
        const data = JSON.parse(fileContent);
        const messages = [];

        Object.values(data).forEach(({ id, defaultMessage }) => {
          messages.push({ id, text: defaultMessage });
        });

        return messages;
      }
    };
