import { namespace } from "../package/packageSettings";

/**
 * Directory in which to store package-related data
 */
export const storageDir = `.${namespace}`;

/**
 * Name of the storage manifest file
 */
export const storageManifestFilename = "manifest.json";

/**
 * Maximum number of versions to keep in storage
 */
export const storageVersionCount = 5;
