import { namespace } from "../package/packageSettings";

/**
 * Default path of the configuration file
 * @type {string}
 */
export const defaultConfigPath = `${namespace}.config.js`;

/**
 * Name of the CLI option for the configuration file path
 * @type {string}
 */
export const configOption = "config";
