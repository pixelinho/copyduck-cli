import { logVersionDiffReport } from "./reportLoggers";
import { VersionDiff } from "../version/versionTypes";
import * as logStreams from "../log/logStreams";

describe("logVersionDiffReport", () => {
  const message1 = { id: "1", text: "abc" };
  const message2 = { id: "2", text: "def" };

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("prints how many messages were removed from the previous version", () => {
    const spy = jest.spyOn(logStreams, "logInfo").mockImplementation();
    const removed = [message1, message2];

    logVersionDiffReport({
      removed,
      created: [],
      updated: []
    } as any);

    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(`${removed.length}`)
    );
  });

  it("prints how many messages were created from the previous version", () => {
    const spy = jest.spyOn(logStreams, "logInfo").mockImplementation();
    const created = [message1, message2];

    logVersionDiffReport({
      removed: [],
      created,
      updated: []
    } as any);

    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(`${created.length}`)
    );
  });

  it("prints how many messages were updated", () => {
    const spy = jest.spyOn(logStreams, "logInfo").mockImplementation();
    const updated = [{ previous: message1, current: message2 }];

    logVersionDiffReport({
      removed: [],
      created: [],
      updated
    } as any);

    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(`${updated.length}`)
    );
  });

  it("prints a message if there was no update", () => {
    const spy = jest.spyOn(logStreams, "logInfo").mockImplementation();

    logVersionDiffReport({
      removed: [],
      created: [],
      updated: []
    } as any);

    expect(spy).toHaveBeenCalledWith(expect.stringContaining("No changes"));
  });
});
