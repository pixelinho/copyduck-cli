import { promises as fsPromises } from "fs";
import { pushVersionToStorageManifest } from "./storageSetters";
import { getStorageVersions } from "./storageGetters";
import { storageVersionCount } from "./storageSettings";

describe("pushVersionToStorageManifest", () => {
  const filename = "file.json";

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("adds the new version to the storage manifest", async () => {
    const version = { timestamp: 1, filename };
    const storageManifest = { versions: [] };

    await pushVersionToStorageManifest(version, storageManifest);

    expect(getStorageVersions(storageManifest)).toHaveLength(1);
  });

  it("cleans old versions", async () => {
    const storageManifest = { versions: [] };
    const spy = jest.spyOn(fsPromises, "unlink").mockResolvedValue();

    for (let i = 0; i < storageVersionCount; i++) {
      getStorageVersions(storageManifest).push({ timestamp: i, filename });
    }

    await pushVersionToStorageManifest(
      { timestamp: storageVersionCount, filename },
      storageManifest
    );

    expect(spy).toHaveBeenCalledTimes(1);
  });
});
