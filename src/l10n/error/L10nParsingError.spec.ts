import L10nParsingError from "./L10nParsingError";

describe("L10nParsingError", () => {
  const file = "/path/";
  const message = "Oupsie";

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("calls `captureStackTrace`", () => {
    const spy = jest.spyOn(Error, "captureStackTrace");

    // tslint:disable-next-line:no-unused-expression
    new L10nParsingError(file, message);

    expect(spy).toHaveBeenCalled();
  });

  it("has a custom name", () => {
    const e = new L10nParsingError(file, message);

    expect(e.name).toEqual("L10nParsingError");
  });

  it("has a custom message", () => {
    const errors = ["message could not be parsed"];
    const e = new L10nParsingError(file, message, errors);

    expect(e.message).toEqual(expect.stringContaining(file));
    expect(e.message).toEqual(expect.stringContaining(message));
    expect(e.message).toEqual(expect.stringContaining(errors[0]));
  });
});
