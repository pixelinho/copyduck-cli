import { Message } from "../message/messageTypes";

export interface Version {
  /**
   * Timestamp at which the version was created
   */
  timestamp: number;
  /**
   * Name of the version master file
   */
  filename: string;
}

export interface VersionDiff {
  /**
   * Messages removed from the previous version
   */
  removed: Message[];
  /**
   * Messages added from the previous version
   */
  created: Message[];
  /**
   * Messages of which text has changed from the previous version
   */
  updated: {
    current: Message;
    previous: Message;
  }[];
}
