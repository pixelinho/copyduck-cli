import { validateStorageManifest } from "./storageValidators";

describe("validateStorageManifest", () => {
  const timestamp = 123;
  const filename = "file.json";

  it("returns an error message if the manifest is not an object", () => {
    const expected = [expect.stringContaining("object")];

    expect(validateStorageManifest(null)).toEqual(expected);
    expect(validateStorageManifest(1)).toEqual(expected);
  });

  it("returns an error message if the manifest has no versions", () => {
    expect(validateStorageManifest({})).toEqual([
      expect.stringContaining("versions")
    ]);
  });

  it("returns an error message if versions are not an array", () => {
    expect(validateStorageManifest({ versions: "" })).toEqual([
      expect.stringContaining("versions")
    ]);
  });

  it("returns errors messages if versions are invalid", () => {
    expect(
      validateStorageManifest({
        versions: [
          {
            timestamp,
            filename
          },
          { timestamp }
        ]
      })
    ).toHaveLength(1);
  });

  it("returns an empty array if the manifest is valid", () => {
    expect(
      validateStorageManifest({
        versions: [
          {
            timestamp,
            filename
          }
        ]
      })
    ).toEqual([]);
  });
});
