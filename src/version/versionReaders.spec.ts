import { promises as fsPromises } from "fs";
import { readVersionFile } from "./versionReaders";

describe("readVersion", () => {
  const filename = "file.json";

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it("calls `readFile` with the proper path", async () => {
    const spy = jest.spyOn(fsPromises, "readFile").mockResolvedValue("{}");

    await readVersionFile({ timestamp: 1, filename });

    expect(spy).toHaveBeenCalledWith(expect.stringContaining(filename), "utf8");
  });

  it("returns the content of the file as an object", async () => {
    const messageMap = { home: "Home" };

    jest
      .spyOn(fsPromises, "readFile")
      .mockResolvedValue(JSON.stringify(messageMap));

    expect(await readVersionFile({ timestamp: 1, filename })).toEqual(
      messageMap
    );
  });
});
