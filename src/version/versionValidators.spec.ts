import { validateVersion } from "./versionValidators";

describe("validateVersion", () => {
  it("returns an error message if the message is not an object", () => {
    const expected = [expect.stringContaining("object")];

    expect(validateVersion(null)).toEqual(expected);
    expect(validateVersion(1)).toEqual(expected);
  });

  it("returns an error message if the message has no timestamp", () => {
    expect(validateVersion({ filename: "file.json" })).toEqual([
      expect.stringContaining("timestamp")
    ]);
  });

  it("returns an error message if the message has no filename", () => {
    expect(validateVersion({ timestamp: 123 })).toEqual([
      expect.stringContaining("filename")
    ]);
  });

  it("returns error messages if timestamp and filename are invalid", () => {
    expect(validateVersion({ timestamp: true, filename: 1 })).toHaveLength(2);
  });

  it("returns an empty array if the version is valid", () => {
    expect(
      validateVersion({
        timestamp: 123,
        filename: "file.json"
      })
    ).toEqual([]);
  });
});
