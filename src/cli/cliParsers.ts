import { CLIOptions } from "./cliTypes";
import { getCLIArguments } from "../process/processGetters";

/**
 * Parses options from command line arguments
 * @example
 * // when running the command `npm run perf --runs 5 --first`,
 * // returns { runs: 5, first: true }
 * parseCLIOptions()
 *
 * @returns {CLIOptions} Pairs of options and their value
 */
export function parseCLIOptions(): CLIOptions {
  const [, , ...options] = getCLIArguments();
  const optionPattern = /^--([a-z]+)$/;
  const result = {};

  for (const [index, item] of options.entries()) {
    const matches = item.match(optionPattern);

    if (matches) {
      const [, option] = matches;
      const nextItem = options[index + 1];

      let value: boolean | number | string = nextItem;

      if (
        typeof nextItem === "undefined" ||
        nextItem.match(optionPattern) ||
        nextItem === "true"
      ) {
        value = true;
      } else if (nextItem === "false") {
        value = false;
      } else if (!Number.isNaN(Number(nextItem))) {
        value = Number(nextItem);
      }

      result[option] = value;
    }
  }

  return result;
}
