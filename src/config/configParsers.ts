import { join } from "path";
import { composeException } from "../log/logHelpers";
import readConfigFile from "./configReader";
import { Config } from "./configTypes";
import { getCurrentWorkingDirectory } from "../process/processGetters";

/**
 * Parses the configuration file and creates a valid configuration object
 * @param {string} path Path to the configuration file
 * @returns {Config} Configuration object
 */
export function parseConfigFile(path: string): Config {
  const { l10nRegex, include, messageMapper } = readConfigFile(path) as Partial<
    Config
  >;

  return {
    l10nRegex: parseL10nRegex(l10nRegex),
    include: parseInclude(include),
    messageMapper: parseMessageMapper(messageMapper)
  };
}

/**
 * Parses the `l10nRegex` configuration property
 * @param {*} value Parsed `l10nRegex` value
 * @returns {RegExp} Valid `l10nRegex` value
 *
 * @throws Will throw an exception if value is not defined or not of the
 * right type
 */
export function parseL10nRegex(value: any): RegExp {
  let l10nRegex;

  if (typeof value === "string") {
    try {
      l10nRegex = new RegExp(value);
    } catch (e) {
      throw composeException(
        new SyntaxError(`l10nRegex must be a valid RegExp`),
        e
      );
    }
  } else if (value instanceof RegExp) {
    l10nRegex = value;
  } else {
    throw new TypeError("`l10nRegex` must be a string or a RegExp");
  }

  return l10nRegex;
}

/**
 * Parses the `include` configuration property and build paths from the current
 * working directory
 * @param {*} value Parsed `include` value
 * @returns {string[]} Valid `include` value
 *
 * @throws Will throw an exception if value is not defined or not of the
 * right type
 */
export function parseInclude(value: any): string[] {
  let include;

  if (typeof value === "string") {
    include = [value];
  } else if (Array.isArray(value)) {
    value.forEach(str => {
      if (typeof str !== "string") {
        throw new TypeError("`include` items must be strings");
      }
    });

    include = value;
  } else {
    throw new TypeError("`include` must be a string or an array");
  }

  return include.map(path => join(getCurrentWorkingDirectory(), path));
}

/**
 * Checks that `messageMapper` configuration property is a function
 * @param {*} value Parsed `messageMapper` value
 * @returns {string[]} `messageMapper` function
 *
 * @throws Will throw an exception if value is not defined or not of the
 * right type
 */
export function parseMessageMapper(value: any): any {
  if (typeof value !== "function") {
    throw new TypeError("`messageMapper` must be a function");
  }

  return value;
}
