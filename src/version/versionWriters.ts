import { promises as fsPromises } from "fs";
import { joinStoragePath } from "../storage/storageHelpers";
import { Version } from "./versionTypes";
import { getVersionFilename } from "./versionGetters";
import { MessageMap } from "../message/messageTypes";
import { StorageManifest } from "../storage/storageTypes";
import { versionBasename } from "./versionSettings";

/**
 * Writes a version file
 * @param {MessageMap} messageMap Content of the new version
 * @returns {Promise<Version>} A promise that resolves with the new version
 */
export async function writeVersionFile(
  messageMap: MessageMap
): Promise<Version> {
  const timestamp = Date.now();
  const filename = `${versionBasename}-${timestamp}.json`;

  await fsPromises.writeFile(
    joinStoragePath(filename),
    JSON.stringify(messageMap, null, 2)
  );

  return {
    timestamp,
    filename
  };
}

/**
 * Delete a version file
 * @param {Version} version Version of which to delete the file
 * @returns {Promise<void>} A promise that resolves when the file is deleted
 */
export async function deleteVersionFile(version: Version): Promise<void> {
  return fsPromises.unlink(joinStoragePath(getVersionFilename(version)));
}
