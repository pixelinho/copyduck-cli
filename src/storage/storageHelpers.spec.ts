import {
  getStoragePath,
  getStorageManifestPath,
  joinStoragePath
} from "./storageHelpers";
import { storageDir, storageManifestFilename } from "./storageSettings";

describe("getStoragePath", () => {
  it("returns the path to the storage directory", () => {
    expect(getStoragePath()).toMatch(new RegExp(`${storageDir}$`));
  });
});

describe("getStorageManifestPath", () => {
  it("returns the path to the storage manifest file", () => {
    expect(getStorageManifestPath()).toMatch(
      new RegExp(`${storageDir}/${storageManifestFilename}$`)
    );
  });
});

describe("joinStoragePath", () => {
  it("joins paths", () => {
    const path = "path/file.json";

    expect(joinStoragePath(path)).toMatch(new RegExp(`${storageDir}/${path}$`));
  });
});
