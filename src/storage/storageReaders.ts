import { promises as fsPromises } from "fs";
import { getStorageManifestPath } from "./storageHelpers";
import { StorageManifest } from "./storageTypes";

/**
 * Reads the storage manifest file
 * @returns {Promise<StorageManifest>} A promise that resolves with the
 * storage manifest
 */
export async function readStorageManifest(): Promise<StorageManifest> {
  return JSON.parse(
    await fsPromises.readFile(getStorageManifestPath(), "utf8")
  ) as StorageManifest;
}
