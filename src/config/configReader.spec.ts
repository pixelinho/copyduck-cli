import readConfigFile from "./configReader";

describe("readConfigFile", () => {
  const modulePath = "./config";

  beforeEach(() => {
    jest.resetModules();
  });

  it("returns what the configuration file exports", () => {
    const data = { a: "b" };

    jest.doMock(modulePath, () => data, { virtual: true });

    expect(readConfigFile(modulePath)).toEqual(data);
  });

  it("throws an exception if the configuration file doesn't export an object", () => {
    jest.doMock(modulePath, () => 1, { virtual: true });

    expect(() => readConfigFile(modulePath)).toThrow();
  });

  it("throws an exception if the configration file cannot be read", () => {
    expect(() => readConfigFile("./ghost-config.js")).toThrow();
  });
});
